module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }

        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },

    isAgeValid: function (age) {
        var regex = /^[0-9]+$/;
        if (age.match(regex) && age >= 18 && age <= 100) {
            return true;
        }
        return false;
    },
    isPasswordValid: function (password) {
        var number = /^[0-9]+$/;
        var conNumber = 0;
        var upChar = /^[A-Z]+$/;
        var conUpChar = false;
        var format = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
        var conFormat = false;
        if (password.length > 7) {
            for (let i = 0; i < password.length; i++) {
                
                if (password[i].match(upChar)) {
                    conUpChar = true;
                }
                if (password[i].match(number)) {
                    conNumber++;
                }
                if (password[i].match(format)) {
                    conFormat = true;
                }
            }
            if (conUpChar == true && conNumber >= 3 && conFormat == true) {
                return true;
            }
            return false;
        }
        return false;
    }

    ,
    isDateValid: function (day, month, year) {
        var ssyear = false;
        var max_day = 0;
        if (year % 400 == 0 && year % 100 == 0) {
            ssyear = true;
        }
        if (month == 2) {
            if (ssyear) {
                max_day = 29;
            } else {
                max_day = 28;
            }
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            max_day = 30;
        } else {
            max_day = 31;
        }

        if (day < 1 || day > max_day || month < 1 || month > 12 || year < 1970 || year > 2020) {
            return false;
        }
        return true;
    }



}
